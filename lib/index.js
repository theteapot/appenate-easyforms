"use strict";

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var fetch = require('node-fetch');

var qs = require('qs'); // Look, I don't know how bable works but requiring the below lets me run
// compiled code, don't judge future me


var regeneratorRuntime = require('regenerator-runtime');

var Appenate =
/*#__PURE__*/
function () {
  function Appenate(_ref) {
    var integrationKey = _ref.integrationKey,
        companyId = _ref.companyId;

    _classCallCheck(this, Appenate);

    if (!integrationKey || !companyId) {
      throw new Error('You must specify an integrationKey and a companyId');
    }

    this.integrationKey = integrationKey;
    this.companyId = companyId;
    this.baseUrl = 'https://secure.appenate.com:443/api/v2'; // Functions for the various appenate endpoints
  }

  _createClass(Appenate, [{
    key: "getDatasource",
    value: function () {
      var _getDatasource = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(_ref2) {
        var id, pageSize, pageNo, _ref2$returnRows, returnRows, response, rows;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                id = _ref2.id, pageSize = _ref2.pageSize, pageNo = _ref2.pageNo, _ref2$returnRows = _ref2.returnRows, returnRows = _ref2$returnRows === void 0 ? true : _ref2$returnRows;
                _context.next = 3;
                return fetch("".concat(this.baseUrl, "/datasource?").concat(qs.stringify({
                  Id: id,
                  PageSize: pageSize,
                  PageNo: pageNo,
                  ReturnRows: returnRows,
                  IntegrationKey: this.integrationKey,
                  CompanyId: this.companyId
                })));

              case 3:
                _context.next = 5;
                return _context.sent.json();

              case 5:
                response = _context.sent;

                if (returnRows) {
                  _context.next = 10;
                  break;
                }

                return _context.abrupt("return", response.DataSource.Headers);

              case 10:
                // Map the headers onto the rows
                rows = response.DataSource.Rows.map(function (row) {
                  row = row.reduce(function (acc, curr, index) {
                    acc[response.DataSource.Headers[index].Name] = curr;
                    return acc;
                  }, {});
                  return row;
                });
                return _context.abrupt("return", rows);

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getDatasource(_x) {
        return _getDatasource.apply(this, arguments);
      }

      return getDatasource;
    }()
  }, {
    key: "putDatasource",
    value: function () {
      var _putDatasource = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(_ref3) {
        var id, _ref3$rows, rows, _ref3$deletedRows, deletedRows, _ref3$newRows, newRows, headers, _map, _map2, mappedRows, mappedDeletedRows, mappedNewRows, response;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                id = _ref3.id, _ref3$rows = _ref3.rows, rows = _ref3$rows === void 0 ? [] : _ref3$rows, _ref3$deletedRows = _ref3.deletedRows, deletedRows = _ref3$deletedRows === void 0 ? [] : _ref3$deletedRows, _ref3$newRows = _ref3.newRows, newRows = _ref3$newRows === void 0 ? [] : _ref3$newRows;
                _context2.next = 3;
                return this.getDatasource({
                  id: id,
                  returnRows: false
                });

              case 3:
                _context2.t0 = function (_ref4) {
                  var Name = _ref4.Name;
                  return Name;
                };

                headers = _context2.sent.map(_context2.t0);
                _map = [rows, deletedRows, newRows].map(function (data, index) {
                  if (data.length > 0) {
                    return data.map(function (row) {
                      return headers.map(function (header) {
                        if (row[header]) {
                          return row[header];
                        } else {
                          return '';
                        }
                      });
                    });
                  } else {
                    return data;
                  }
                }), _map2 = _slicedToArray(_map, 3), mappedRows = _map2[0], mappedDeletedRows = _map2[1], mappedNewRows = _map2[2];
                _context2.next = 8;
                return fetch("".concat(this.baseUrl, "/datasource"), {
                  method: 'PUT',
                  headers: {
                    'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                    Id: id,
                    CompanyId: this.companyId,
                    IntegrationKey: this.integrationKey,
                    Rows: mappedRows,
                    NewRows: mappedNewRows,
                    DeletedRows: mappedDeletedRows
                  })
                });

              case 8:
                _context2.next = 10;
                return _context2.sent.json();

              case 10:
                response = _context2.sent;
                return _context2.abrupt("return", response);

              case 12:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function putDatasource(_x2) {
        return _putDatasource.apply(this, arguments);
      }

      return putDatasource;
    }()
  }]);

  return Appenate;
}();

exports.Appenate = Appenate;