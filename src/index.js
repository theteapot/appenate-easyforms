const fetch = require('node-fetch')
const qs = require('qs')
// Look, I don't know how bable works but requiring the below lets me run
// compiled code, don't judge future me
const regeneratorRuntime = require('regenerator-runtime')

class Appenate {
  constructor ({ integrationKey, companyId }) {
    if (!integrationKey || !companyId) {
      throw new Error('You must specify an integrationKey and a companyId')
    }

    this.integrationKey = integrationKey
    this.companyId = companyId
    this.baseUrl = 'https://secure.appenate.com:443/api/v2'

    // Functions for the various appenate endpoints
  }

  async getDatasource ({ id, pageSize, pageNo, returnRows = true }) {
    let response = await (await fetch(
      `${this.baseUrl}/datasource?${qs.stringify({
        Id: id,
        PageSize: pageSize,
        PageNo: pageNo,
        ReturnRows: returnRows,
        IntegrationKey: this.integrationKey,
        CompanyId: this.companyId
      })}`
    )).json()

    // If we are not returning rows we will instead return the headers array
    if (!returnRows) {
      return response.DataSource.Headers
    } else {
      // Map the headers onto the rows
      let rows = response.DataSource.Rows.map((row) => {
        row = row.reduce((acc, curr, index) => {
          acc[response.DataSource.Headers[index].Name] = curr
          return acc
        }, {})
        return row
      })

      return rows
    }
  }

  async putDatasource ({ id, rows = [], deletedRows = [], newRows = [] }) {
    // Get the headers so we know how to structure our requests
    let headers = (await this.getDatasource({ id, returnRows: false })).map(
      ({ Name }) => Name
    )

    let [mappedRows, mappedDeletedRows, mappedNewRows] = [
      rows,
      deletedRows,
      newRows
    ].map((data, index) => {
      if (data.length > 0) {
        return data.map((row) =>
          headers.map((header) => {
            if (row[header]) {
              return row[header]
            } else {
              return ''
            }
          })
        )
      } else {
        return data
      }
    })

    let response = await (await fetch(`${this.baseUrl}/datasource`, {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        Id: id,
        CompanyId: this.companyId,
        IntegrationKey: this.integrationKey,
        Rows: mappedRows,
        NewRows: mappedNewRows,
        DeletedRows: mappedDeletedRows
      })
    })).json()

    return response
  }
}

exports.Appenate = Appenate
