require('dotenv').config()

const assert = require('assert')
const { Appenate } = require('../index')
const { ID, COMPANY_ID, INTEGRATION_KEY } = process.env

describe('Testing datasource functions', () => {
  let appenate = new Appenate({
    companyId: COMPANY_ID,
    integrationKey: INTEGRATION_KEY
  })

  it('Should get a datasource', async () => {
    let response = await appenate.getDatasource({ id: ID })
    assert(Array.isArray(response), `Expected array, got ${typeof response}`)
  })

  it('Should update a datasource', async () => {
    let preUpdateLength = (await appenate.getDatasource({ id: ID })).length

    await appenate.putDatasource({
      id: ID,
      newRows: [{ ProjectNumber: '3456' }]
    })

    let postUpdateLength = (await appenate.getDatasource({ id: ID })).length
    assert(
      postUpdateLength === preUpdateLength + 1,
      `Expected total length to be ${preUpdateLength +
        1}, got ${postUpdateLength}`
    )
  })

  it('Should delete a row from a datasource', async () => {
    let preUpdateLength = (await appenate.getDatasource({ id: ID })).length
    await appenate.putDatasource({
      id: ID,
      deletedRows: [{ ProjectNumber: '3456' }]
    })

    let postUpdateLength = (await appenate.getDatasource({ id: ID })).length
    assert(
      postUpdateLength === preUpdateLength - 1,
      `Expected total length to be ${preUpdateLength +
        1}, got ${postUpdateLength}`
    )
  })
})
