const assert = require('assert')

describe('Should test if the compiled code loads successfully', () => {
  it('Should load without error', () => {
    const { Appenate } = require('../../lib')
    assert(
      typeof Appenate === 'function',
      `Expected 'function' got ${typeof Appenate}`
    )
  })
})
